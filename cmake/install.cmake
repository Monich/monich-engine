file(GLOB_RECURSE LINK_FILES "cmake/Link/*.cmake")
file(GLOB_RECURSE FIND_FILES "cmake/Modules/*.cmake")

install(TARGETS monich-engine ARCHIVE DESTINATION lib)

install(FILES cmake/monich-engineConfig.cmake DESTINATION lib/cmake/monich-engine)
install(FILES ${LINK_FILES} DESTINATION lib/cmake/monich-engine/Link)
install(FILES ${FIND_FILES} DESTINATION lib/cmake/monich-engine/Modules)
install(FILES ${ME_INCS} DESTINATION include/monich-engine)
