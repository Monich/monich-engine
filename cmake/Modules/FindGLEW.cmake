FIND_PATH(GLEW_INCLUDE_DIR
	NAME GL/glew.h
	PATHS
    deps/include
    /usr/include
    "C:/Program Files/glew/include"
  DOC "The directory containing GLEW include files."
)

FIND_LIBRARY(GLEW_LIBRARY
    NAMES glew32s GLEW
    PATHS
    deps/lib
    /usr/lib
    "C:/Program Files/glew/lib/*/x64"
    DOC "The GLEW compiled library."
)

if(GLEW_INCLUDE_DIR AND GLEW_LIBRARY)
    set(GLEW_FOUND 1)
else(GLEW_INCLUDE_DIR AND GLEW_LIBRARY)
    message(FATAL_ERROR "Couldn't find GLEW library.")
endif(GLEW_INCLUDE_DIR AND GLEW_LIBRARY)
