FIND_PATH(GLFW3_INCLUDE_DIR
	NAME GLFW/glfw3.h
	PATHS
    deps/include
    /usr/include
    "C:/Program Files/glfw/include"
  DOC "The directory containing GLFW3 include files."
)

FIND_LIBRARY(GLFW3_LIBRARY
    NAME glfw glfw3
    PATHS
    deps/lib
    /usr/lib
    "C:/Program Files/glfw/lib"
    DOC "The GLFW3 compiled library."
)

if(GLFW3_INCLUDE_DIR AND GLFW3_LIBRARY)
    set(${GLFW3_FOUND} 1)
else(GLFW3_INCLUDE_DIR AND GLFW3_LIBRARY)
    message(FATAL_ERROR "Couldn't locate GLFW3 library")
endif(GLFW3_INCLUDE_DIR AND GLFW3_LIBRARY)

