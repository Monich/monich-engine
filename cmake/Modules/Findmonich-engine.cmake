FIND_PATH(ME_INCLUDE_DIR
	NAME monich-engine/ME.hpp
	PATHS
    deps/include
    /usr/include
    /usr/local/include
    "C:/Program Files/monich-engine/include"
    "${ME_DIR}/include"
  DOC "The directory containing Monich Engine include files."
)

FIND_LIBRARY(ME_LIBRARY
    NAME monich-engine
    PATHS
    deps/lib
    /usr/lib
    /usr/local/lib
    "C:/Program Files/monich-engine/lib"
    "${ME_DIR}/lib"
    DOC "Monich ENgine compiled library."
)

if(ME_INCLUDE_DIR AND ME_LIBRARY)
    set(${ME_FOUND} 1)
    mark_as_advanced(FORCE ME_DIR)
else(ME_INCLUDE_DIR AND ME_LIBRARY)
    option(ME_DIR "Path to external Monich Engine installation directory" PATH)
    mark_as_advanced(CLEAR ME_DIR)
    message(FATAL_ERROR "Couldn't locate MonichEngine library! Include dir: ${ME_INCLUDE_DIR} - lib: ${ME_LIBRARY}")
endif(ME_INCLUDE_DIR AND ME_LIBRARY)

