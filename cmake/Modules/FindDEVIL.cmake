FIND_PATH(DEVIL_INCLUDE_DIR
	NAME IL/il.h
	PATHS
    deps/include
    /usr/include
    "C:/Program Files/DevIL/include"
  DOC "The directory containing DevIL include files."
)

FIND_LIBRARY(DEVIL_LIBRARY_1
    NAMES IL DevIL
    PATHS
    deps/lib
    /usr/lib
    "C:/Program Files/DevIL/lib/*/*"
    DOC "The DevIL compiled library."
)

FIND_LIBRARY(DEVIL_LIBRARY_2
    NAMES ILU
    PATHS
    deps/lib
    /usr/lib
    "C:/Program Files/DevIL/lib/*/*"
    DOC "The DevIL utility library."
)

FIND_LIBRARY(DEVIL_LIBRARY_3
    NAMES ILUT
    PATHS
    deps/lib
    /usr/lib
    "C:/Program Files/DevIL/lib/*/*"
    DOC "The DevIL UT library."
)

if(DEVIL_INCLUDE_DIR AND DEVIL_LIBRARY_1 AND DEVIL_LIBRARY_2 AND DEVIL_LIBRARY_3)
    set(DEVIL_FOUND 1)
else(DEVIL_INCLUDE_DIR AND DEVIL_LIBRARY_1 AND DEVIL_LIBRARY_2 AND DEVIL_LIBRARY_3)
    message(FATAL_ERROR "Couldn't locate DevIL library.	")
endif(DEVIL_INCLUDE_DIR AND DEVIL_LIBRARY_1 AND DEVIL_LIBRARY_2 AND DEVIL_LIBRARY_3)

