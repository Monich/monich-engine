message(STATUS "Configuring MonichEngine")

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_LIST_DIR}/Modules/")

message(STATUS "Looking for external libraries...")
include(${CMAKE_CURRENT_LIST_DIR}/Link/devil.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Link/glew.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Link/glfw3.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Link/glm.cmake)

if(NOT ME_LINK_SOURCE)
    include(${CMAKE_CURRENT_LIST_DIR}/Link/me.cmake)
    link_libraries(${ME_LIBRARY})
endif(NOT ME_LINK_SOURCE)

message(STATUS "All libraries found, linking engine to the project")

link_libraries(${DEVIL_LIBRARY_1} ${DEVIL_LIBRARY_2} ${DEVIL_LIBRARY_3})
link_libraries(${GLEW_LIBRARY})
link_libraries(${GLFW3_LIBRARY})

if(NOT WIN32)
    link_libraries(GL)
else(NOT WIN32)
    add_definitions(-DGLEW_STATIC)
    link_libraries(opengl32)
endif(NOT WIN32)

message(STATUS "Configuration of MonichEngine is finished")

