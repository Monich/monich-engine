# Find needed libraries
message(STATUS "Looking for Monich Engine compiled library...")
find_package(monich-engine REQUIRED)

message(STATUS "Monich Engine library found: ${ME_LIBRARY}")
message(STATUS "Monich Engine include folder found: ${ME_INCLUDE_DIR}")

include_directories(${ME_INCLUDE_DIR})

mark_as_advanced(ME_INCLUDE_DIR ME_LIBRARY)
