# Disable MS warnings for secure functions
add_definitions(-D_CRT_SECURE_NO_WARNINGS)

# Set newest compiler version for Linux
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11")

# Set modules path
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")

# Find needed libraries
message(STATUS "Looking for external libraries")
include(cmake/Link/devil.cmake)
include(cmake/Link/glew.cmake)
include(cmake/Link/glfw3.cmake)
include(cmake/Link/glm.cmake)
message(STATUS "All libraries found")

# Include install
include(cmake/install.cmake)
