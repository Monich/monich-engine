# Monich Engine
Monich Engine is a simple graphical engine. This is the latest release supposed to be supported from now on. It uses OpenGL and bunch of external libraries for work.

# Building Monich Engine
Monich Engine relies on external libraries for its build. You will need a C++ compilator and CMake to create a make file or VS project.
For Linux you will need following packages: libglfw3-dev libglew-dev libdevil-dev libglm-dev.

For Windows install following libraries: 
 - GLEW:  http://glew.sourceforge.net/
 - GLFW:  https://www.glfw.org/
 - DevIL: http://openil.sourceforge.net/
 - GLM:   https://glm.g-truc.net/0.9.9/index.html
CMake should recognize the libraries if you put them in Program Files directory. 

# Using Monich Engine
You can use Monich Engine using install build target or using plain source code. Engine provides CMake config file to link it with your project.

## Via installation
Install Monich Engine using INSTALL target in make file on in Visual Studio solution. To change default installation directory change CMAKE_INSTALL_PREFIX variable in CMake build. Include config file at libs/cmake/monich-engineConfig.cmake. If you change install directory you have to make sure config file will find binaries. Set ME_DIR to instalation path before including the config file

## Via source
This is prefered method for debugging. Set ME_LINK_SOURCE to true before including the config file. It is stored at cmake/monich-engineConfig.cmake.
